import time
from colorama import Fore, Style
import logging

import sieve
import generator
import functions

'''
colorscheme
MAGENTA - Inputs
RED - Warnings and errors
GREEN - Successful operations
YELLOW - System notifications
'''

while True:
    # initialization of logger
    logger = logging.getLogger('dev')
    logger.setLevel(logging.DEBUG)
    logging.basicConfig(filename='logs/' + time.strftime("%Y%m%d_%H%M%S") + '.log', filemode='w',
                        format='%(asctime)s - %(filename)s %(levelname)s: %(message)s', datefmt='%d-%b-%y %H:%M:%S')
    print(
        '''
1. Testing prime numbers
2. Generating prime numbers
3. Print prime numbers
4. End program
''')
    choose = input(Fore.MAGENTA + "Enter your choice: " + Style.RESET_ALL)  # choice of activity
    logger.info('Initial menu displayed')

    if choose == '1':
        logger.info(f'Option {choose} - testing picked')
        # choice of writing  or inserting the number for further testing
        choice = input(Fore.MAGENTA + "Would you like to write(1) or load the number from file(2): " + Style.RESET_ALL)
        logger.info('Choice between writing or loading number for testing')

        if choice == '1':
            logger.info(f'Option {choose} - inputting number manually')

            number = functions.check_number()  # verification of number
            if number is None:
                continue  # if it's not a number then continue

        elif choice == '2':
            logger.info(f'Option {choose} - loading number from a file')

            number = functions.file_load()  # loading number from a file
            if number is None:
                continue  # if it's not a number then continue

        else:
            print(Fore.RED + "There is no option like that!\n" + Style.RESET_ALL)
            logger.error('Number input choice not available')
            continue

        print(
            '''
    Which test would you like to use:

    1. Fermat primality test
    2. Miller-Rabin primality test
    3. Simple primality test
    4. Fermat + Miller-Rabin primality test
''')
        # choice of test used for testing
        logger.info('Tests menu displayed')
        functions.testing(number)  # calling testing function
        functions.file_save(number)  # calling saving function

    elif choose == '2':
        logger.info(f'Option {choose} - generating picked')

        print(
            '''
    Which generator would you like to use:
    
    1. n-bit generator
    2. Mersenne number generator
''')

        logger.info('Generators menu displayed')
        choice = input(Fore.MAGENTA + "Enter your choice: " + Style.RESET_ALL)  # choice of generator used

        if choice == '1':
            logger.info(f'Option {choose} - generating using n-bit generator')

            number = functions.check_key()  # verification of key size
            if number is None:
                continue  # if the key size is wrong then continue
            startTime = time.time()  # starting counter for measuring time of generating
            logger.debug('Time counter for n-bit generating initialized')
            generated_prime = generator.generate_prime(number)  # calling function for generating
            if generated_prime is None:
                continue  # if prime was not generated right continue
            # print generated prime number and time it took to generate the number
            print(Fore.GREEN + "Generated prime number is " + Style.RESET_ALL + str(generated_prime))
            print(Fore.YELLOW + "Completed in " + str(time.time() - startTime) + " seconds.\n" + Style.RESET_ALL)
            logger.debug(f'Time needed for this operation was {str(time.time() - startTime)}')

        elif choice == '2':
            logger.info(f'Option {choose} - Mersenne number generator')

            number = functions.check_key_m()  # verification inserted number
            if number is None:
                continue
            startTime = time.time()  # starting counter for measuring time of generating
            logger.debug('Time counter for Mersenne number generating initialized')
            generated_prime = generator.generate_mersenne(number)  # calling Mersenne prime generator
            if generated_prime is None:
                continue  # if prime was not generated right continue
            # print generated prime number and time it took to generate the number
            print(Fore.GREEN + "Generated prime number is " + Style.RESET_ALL + str(generated_prime))
            print(Fore.YELLOW + "Completed in " + str(time.time() - startTime) + " seconds.\n" + Style.RESET_ALL)
            logger.debug(f'Time needed for this operation was {str(time.time() - startTime)}')

        else:
            print(Fore.RED + "There is no option like that!\n" + Style.RESET_ALL)
            logger.error('Generator option not available')
            continue

        functions.file_save(generated_prime)  # calling saving function

    elif choose == '3':
        logger.info(f'Option {choose} - printing picked')

        number = functions.check_number()  # verification of number
        if number is None:
            continue  # if it's not a number then continue
        startTime = time.time()  # starting counter for measuring time of generating
        logger.debug('Time counter for n-bit generating initialized')
        sieve.sieve(number)  # calling sieve function
        # print time it took to generate the sieve
        print(Fore.YELLOW + "Completed in " + str(time.time() - startTime) + " seconds." + Style.RESET_ALL)
        logger.debug(f'Time needed for this operation was {str(time.time() - startTime)}')

    elif choose == '4':
        logger.info(f'Option {choose} - program shut down')

        print(Fore.YELLOW + "Program shutting down...\n" + Style.RESET_ALL)  # program will shut down
        break

    else:
        print(Fore.RED + "There is no option like that!\n" + Style.RESET_ALL)
        logger.error('Activity choice not available')
        continue
