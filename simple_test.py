from math import sqrt


def simple_prime_test(n):

    if n < 2:
        return False

    if n == 2:
        return True

    if n % 2 == 0:
        return False

    i = 3
    while i <= int(sqrt(n) + 1):  # loop will go until number reaches the square root of number + 1

        if n % i == 0:  # if remainder of number divided  with i is equal to 0 then it's not a prime
            return False
        i += 2  # even numbers aren't prime by default therefore we try with odd ones only

    return True
