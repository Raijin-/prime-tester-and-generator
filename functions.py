import time
from colorama import Fore, Style
from os.path import exists as file_exists
import logging

import fermat
import miller_rabin
import simple_test

logger = logging.getLogger('dev')
logger.setLevel(logging.DEBUG)


def testing(number):
    logger.info('Testing of prime numbers started')

    test = input(Fore.MAGENTA + "Enter your choice: " + Style.RESET_ALL)
    logger.info('Choice of primality test')
    start_time = time.time()  # starting counter for measuring time of testing
    logger.debug('Time counter for testing initialized')

    if test == '1':
        logger.info(f'Option {test} - Fermat\'s test started')

        if fermat.fermat_test(int(number)):  # calling fermat function and if it's true then it's prime number
            logger.info('Result of Fermat\'s test - PRIME number')
            print(Fore.GREEN + "Number " + Style.RESET_ALL + str(number) +
                  Fore.GREEN + "\nis a prime number!" + Style.RESET_ALL)

        elif not fermat.fermat_test(int(number)):  # calling fermat function and if it's true then it's composite number
            logger.info('Result of Fermat\'s test - COMPOSITE number')
            print(Fore.GREEN + "Number " + Style.RESET_ALL + str(number) +
                  Fore.GREEN + "\nis a composite number!" + Style.RESET_ALL)
        # print time it took to test the number
        print(Fore.YELLOW + "Completed in " + str(time.time() - start_time) + " seconds.\n" + Style.RESET_ALL)
        logger.debug(f'Time needed for this operation was {str(time.time() - start_time)}')

    elif test == '2':
        logger.info(f'Option {test} - Miller-Rabin\'s test started')

        if miller_rabin.miller_rabin_test(int(number)):  # calling MB function > it's prime number
            logger.info('Result of Miller-Rabin\'s test - PRIME number')
            print(Fore.GREEN + "Number " + Style.RESET_ALL + str(number) +
                  Fore.GREEN + "\nis a prime number!" + Style.RESET_ALL)

        elif not miller_rabin.miller_rabin_test(int(number)):  # calling MB function > it's composite number
            logger.info('Result of Miller-Rabin\'s test - COMPOSITE number')
            print(Fore.GREEN + "Number " + Style.RESET_ALL + str(number) +
                  Fore.GREEN + "\nis a composite number!" + Style.RESET_ALL)
        # print time it took to test the number
        print(Fore.YELLOW + "Completed in " + str(time.time() - start_time) + " seconds.\n" + Style.RESET_ALL)
        logger.debug(f'Time needed for this operation was {str(time.time() - start_time)}')

    elif test == '3':
        logger.info(f'Option {test} - Simple primality test started')

        if int(number) > 2 ** 50:  # program can't handle numbers bigger than 2**50, therefore it  will throw a warning
            logger.warning('Inserted number is greater than allowed size 2**50')
            print(Fore.YELLOW + "Simple primality test can't handle numbers bigger than 2^50 because of difficulty "
                                "of the calculation.\nTherefore, this test isn't good for testing big numbers, "
                                "but it's 100% correct for small numbers.\n" + Style.RESET_ALL)
            testing(number)

        elif simple_test.simple_prime_test(int(number)):  # calling Simple test function > it's prime number
            logger.info('Result of Simple primality test - PRIME number')
            print(Fore.GREEN + "Number " + Style.RESET_ALL + str(number) +
                  Fore.GREEN + "\nis a prime number!" + Style.RESET_ALL)
            # print time it took to test the number
            print(Fore.YELLOW + "Completed in " + str(time.time() - start_time) + " seconds.\n" + Style.RESET_ALL)
            logger.debug(f'Time needed for this operation was {str(time.time() - start_time)}')

        elif not simple_test.simple_prime_test(int(number)):  # calling Simple test function > it's composite number
            logger.info('Result of Simple primality test - COMPOSITE number')
            print(Fore.GREEN + "Number " + Style.RESET_ALL + str(number) +
                  Fore.GREEN + "\nis a composite number!" + Style.RESET_ALL)
            # print time it took to test the number
            print(Fore.YELLOW + "Completed in " + str(time.time() - start_time) + " seconds.\n" + Style.RESET_ALL)
            logger.debug(f'Time needed for this operation was {str(time.time() - start_time)}')

    elif test == '4':
        logger.info(f'Option {test} - Complex test started')
        # calling Fermat and MB function > it's prime number
        if fermat.fermat_test(int(number)) and miller_rabin.miller_rabin_test(int(number)):
            logger.info('Result of Simple complex test - PRIME number')
            print(Fore.GREEN + "Number " + Style.RESET_ALL + str(number) +
                  Fore.GREEN + "\nis a prime number!" + Style.RESET_ALL)
        # calling Fermat and MB function > it's composite number
        elif not fermat.fermat_test(int(number)) and not miller_rabin.miller_rabin_test(int(number)):
            logger.info('Result of Simple complex test - COMPOSITE number')
            print(Fore.GREEN + "Number " + Style.RESET_ALL + str(number) +
                  Fore.GREEN + "\nis a composite number!" + Style.RESET_ALL)
        # print time it took to test the number
        print(Fore.YELLOW + "Completed in " + str(time.time() - start_time) + " seconds.\n" + Style.RESET_ALL)
        logger.debug(f'Time needed for this operation was {str(time.time() - start_time)}')

    else:
        print(Fore.RED + "There is no option like that!\n" + Style.RESET_ALL)
        logger.error('Test choice not available')
        testing(number)


def check_number():
    logger.info('Number verification function started')

    try:
        number = int(input(Fore.MAGENTA + "Insert number: " + Style.RESET_ALL))  # verification of data type == int
        if number < 1:  # numbers lower than 1 can't be inserted
            logger.error('Inserted number lower than 1')
            print(Fore.RED + "Inserted number can't be lower than 1" + Style.RESET_ALL)
            return None

        else:
            logger.info(f'Number {number} was successfully inserted')
            return number

    except:
        print(Fore.RED + "Insert only numbers!\n" + Style.RESET_ALL)
        logger.error('Number wasn\'t inserted')
        return None


def check_key():
    logger.info('Key verification function for n-bit numbers started')

    try:
        number = int(input(Fore.MAGENTA + "Insert key size: " + Style.RESET_ALL))  # verification of data type == int
        logger.info(f'Key size {number} was successfully inserted')
        return number

    except:
        print(Fore.RED + "Insert only numbers!\n" + Style.RESET_ALL)
        logger.error('Number wasn\'t inserted')
        return None


def check_key_m():
    logger.info('Key verification function for Mersenne numbers started')

    try:  # verification of data type == int
        number = int(input(Fore.MAGENTA + "Insert number size (max. 12): " + Style.RESET_ALL))
        logger.info(f'Number size {number} was successfully inserted')
        return number

    except:
        print(Fore.RED + "Insert only numbers!\n" + Style.RESET_ALL)
        logger.error('Number wasn\'t inserted')
        return None


def file_load():
    logger.info('File loading function started')

    file_name = input(Fore.MAGENTA + "Insert file name: " + Style.RESET_ALL)  # file name input
    logger.info(f'File {file_name} is being loaded')

    try:
        file_number = open('numbers/' + file_name + '.txt', 'r')  # opening the file in reading mode
        number = file_number.read()  # storing number from the file
        logger.info(f'Number was successfully loaded from {file_name} file')
        print(Fore.YELLOW + "Number loaded from the file is " + Style.RESET_ALL + number)
        file_number.close()  # closing the file so it doesn't cause trouble
        return number

    except:
        logger.error(f'File {file_name} cannot be opened')
        print(Fore.RED + "File cannot be opened!" + Style.RESET_ALL)
        return None


def file_save(number):
    logger.info('File saving function started')
    # option if you want to save the number in a file
    pick = input(Fore.MAGENTA + "Would you like to save the number in a file? (y/n): " + Style.RESET_ALL)
    logger.info('Saving number in a file')

    if pick == 'y':
        logger.info(f'Choice {pick} was made - number proceeding for saving')

        save_file = input(Fore.MAGENTA + "Name your file: " + Style.RESET_ALL)  # naming a file
        logger.info('File naming input')

        if not save_file.strip():  # removing white spaces and checking if the file name is empty
            logger.warning('File name is empty')
            print(Fore.RED + "The file name cannot be empty!\n" + Style.RESET_ALL)
            file_save(number)

        else:
            logger.info(f'Number saving in {save_file} file')

            if file_exists('numbers/' + save_file + '.txt'):  # checking if the file exists
                logger.debug('File with similar name exists')

                choose = input(Fore.MAGENTA + "This file already exists, do you want to rewrite contents of the file? "
                                              "(y/n): " + Style.RESET_ALL)
                logger.debug('Rewriting contents of the file')

                if choose == 'y':  # if the file exists then rewrite it
                    logger.info(f'Option {choose} picked - rewriting contents of {save_file} file')
                    new_file = open('numbers/' + save_file + '.txt', 'w')  # opening the file in writing mode
                    new_file.write(str(number))  # writing a number in the file
                    logger.info(f'Number was successfully saved in {save_file} file')
                    print(Fore.YELLOW + "Number " + Style.RESET_ALL + str(number) + Fore.YELLOW + "\nwas successfully "
                                        "saved!" + Style.RESET_ALL)
                    new_file.close()  # closing the file so it doesn't cause trouble
                    logger.debug(f'File {save_file} was closed')

                elif choose == "n":  # don't rewrite existing file and go back to start of function
                    logger.info(f'Option {choose} picked - choose different name')
                    print(Fore.YELLOW + "Choose different name!\n" + Style.RESET_ALL)
                    file_save(number)

                else:
                    print(Fore.RED + "There is no option like that!\n" + Style.RESET_ALL)
                    logging.error('Chosen option not available')
                    file_save(number)

            else:
                logger.debug(f'Created {save_file} file')  # if file doesn't exist then save it
                new_file = open('numbers/' + save_file + '.txt', 'w')
                new_file.write(str(number))  # writing a number in the file
                logger.info(f'Number was successfully saved in {save_file} file')
                print(Fore.YELLOW + "Number " + Style.RESET_ALL + str(number) + Fore.YELLOW + "\nwas successfully "
                                    "saved!" + Style.RESET_ALL)
                new_file.close()  # closing the file so it doesn't cause trouble
                logger.debug(f'File {save_file} was closed')

    elif pick == 'n':  # option n picked = the number won't be saved to file
        logger.info(f'Choice {pick} was made - number wasn\'t saved')
        print(Fore.YELLOW + "Number wasn't saved!" + Style.RESET_ALL)

    else:
        print(Fore.RED + "There is no option like that!\n" + Style.RESET_ALL)
        logger.error('File saving choice not available')
        file_save(number)
