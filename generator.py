import random
from colorama import Fore, Style
import logging

import fermat
import miller_rabin

logger = logging.getLogger('dev')
logger.setLevel(logging.DEBUG)


def generate_prime(key):
    logger.info('Generating prime numbers function started')

    if key <= 1:
        logger.error('Key size lower than 2')
        print(Fore.RED + "Key size must be 2 or bigger!\n" + Style.RESET_ALL)  # key size must be above 2
        return None

    else:
        logger.info('Generating a number')
        while True:
            number = random.randrange(2 ** (key - 1), 2 ** key)  # random number in the range
            if fermat.fermat_test(number) and miller_rabin.miller_rabin_test(number):  # testing the random number
                logger.info('Prime number successfully generated')
                return number  # if both tests are true then return the number


def generate_mersenne(key):  # the user enters a key, the larger the number, the larger generated Mersenne prime, max 12
    logger.info('Generating Mersenne prime numbers function started')

    if key <= 1:
        logger.error('Key size lower than 2')
        print(Fore.RED + "Key size must be 2 or bigger!\n" + Style.RESET_ALL)  # key size must be above 2
        return None

    else:
        logger.info('Generating a number')
        while True:
            number = random.randrange(2 ** (key - 1), 2 ** key)  # random number in the range
            s = 4  # pre-agreed starting value
            m = ((2 ** number) - 1)  # A Mersenne prime is a prime number that is one less than an integer power of two
            for i in range(number - 2):  # range from number - 2
                s = ((s * s) - 2) % m  # (s^2 - 2 mod(m)
            if s == 0:  # if s is 0 at the end, then it is a mersenne number
                logger.info('Prime number successfully generated')
                return m  # return mersenne number
