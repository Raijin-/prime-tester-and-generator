import random


def miller_rabin_test(n):

    if n == 1:
        return False

    if n == 2:
        return True

    if n % 2 == 0:
        return False

    r, s = 0, n - 1  # first we write n as 2^r*s, s is odd
    
    while s % 2 == 0:  # keep dividing s by two until the result is odd
        r += 1
        s //= 2
        
    for i in range(5):  # determines the accuracy of the test
        
        a = random.randrange(2, n - 1)  # then, we choose a random integer modulo n
        x = pow(a, s, n)  # let x = a^s mod n
        
        if x == 1 or x == n - 1:
            continue
        for j in range(r - 1):  # start squaring until we hit the (n-1)st power
            
            x = pow(x, 2, n)  # x = x^2 mod n
            if x == n - 1:
                break 
        else:
            return False
    return True
