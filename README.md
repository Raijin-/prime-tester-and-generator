# Prime-tester-and-generator

Program pre generovanie, testovanie a vypisovanie veľkých prvočísel, 2048b.

## Náležitosti pre správne fungovanie programu

- Treba nainštalovať knižnicu colorama pomocou **pip install colorama** v cmd
- Pre správne fungovanie treba pracovať v prostredí PyCharm Community od JetBrains _(zadarmo)_
- Správne zobrazenie farieb funguje iba v prostredí PyCharm. V cmd alebo inom prostredí sa farby nezobrazia správne
- Taktiež v prostredí PyCharm je možné testovať väčšia čísla ako napríklad jednoduchý Python IDLE _(Sieve v Python IDLE zvládol maximálne 5 ciferné čísla zatiaľ čo PyCharm až 8 ciferné)_
