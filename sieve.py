import logging

logger = logging.getLogger('dev')
logger.setLevel(logging.DEBUG)


def sieve(n):
    logger.info('Sieve function for printing prime numbers started')
    # List of booleans all initially set to "True"; Number of booleans is equal to the number we test
    bool_list = [True]*(n+1)
    fin_list = []  # List that we output at the end
    mult = 2  # Variable that is used when removing multiples of number
    
    while mult*mult <= n:  # Loop that works until the number is less than the input
        
        if bool_list[mult]:  # If the number is in list
            
            for i in range(mult*mult, n+1, mult):  # Loop for removing multiples of number
                bool_list[i] = False  # All multiples are set to "False"
                
        mult += 1
        
    for mult in range(2, n+1):  # Loop that inserts all "True" booleans to a different list
        
        if bool_list[mult]:  # If the position is "True"
            fin_list.append(mult)  # It appends it to the fin_list
            
    print(fin_list)
    logger.info('Sieve was successfully printed')
