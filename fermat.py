import random


def fermat_test(n):

    if n == 1:
        return False

    if n == 2:
        return True
    
    if n % 2 == 0:
        return False

    if n > 1:

        for i in range(5):  # below code will happen 5 times

            random_number = random.randint(2, n) - 1  # find random number in range 2 to n and do - 1
            if pow(random_number, n - 1, n) != 1:  # if remainder of found number isn't equal to 1 then it's not a prime
                return False
        return True
    else:
        return False
